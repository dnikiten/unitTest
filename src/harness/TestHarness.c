#include "TestHarness.h"

pthread_cond_t      cond  = PTHREAD_COND_INITIALIZER;
pthread_mutex_t     mutex = PTHREAD_MUTEX_INITIALIZER;

struct threadArg {
    int     waitTime;
    pid_t   workerPID;
    int     testNum;
    int     subTest;
};

static bool workerDone = false;

void catchUsr(int sig_num)
{
    /* re-set the signal handler again to catch_int, for next time */
    signal(SIGINT, catchUsr);
    /* and print the message */
    // printf("SIGUSR1 caught");
    fflush(stdout);
}

SubTestRec getSubDataFromChild(int* pipefd){
    
    SubTestRec tmpRec;
    int passed;
    int len;
    
    close(pipefd[1]); // close the write-end of the pipe, I'm not going to use it
    read(pipefd[0], &passed, sizeof(int));
    read(pipefd[0], &len, sizeof(int));
    
    if (passed < 0 || passed > 1 ||
        len <= 0 || len > 2000){
        tmpRec.passed = -1;
        return tmpRec;
    }


    tmpRec.passed = passed;
    char* tmp = malloc(len);
    read(pipefd[0],tmp, len);
    tmpRec.feedback = tmp;
    
    
    return tmpRec;
}

void sendSubDataToParent(int* pipefd, SubTestRec* tmpRec){
    close(pipefd[0]); // close the read-end of the pipe, I'm not going to use it
    //Send result
    write(pipefd[1], &tmpRec->passed, sizeof(testResult));
    
    //Send feedback
    int tmpLen = strlen(tmpRec->feedback)+1;
    write(pipefd[1], (const char*)&tmpLen, sizeof(int));
    write(pipefd[1], tmpRec->feedback, tmpLen);
    close(pipefd[1]);
}

void *threadfunc(void *parm)
{
    int rc;
    struct timespec ts, tsRem;
    struct timeval tp;
    struct threadArg *args = parm;

    // printf("Timer thread:  starting\n");

    // rc = pthread_mutex_lock(&mutex);
    // printf("Timer thread: locked mutex\n");
    
    gettimeofday(&tp, NULL);

    ts.tv_sec = args->waitTime;
    ts.tv_nsec = 0;

    pthread_mutex_lock(&mutex); 
    // printf ("Timer thread: locked mutex\n");
    if (!workerDone){
        pthread_mutex_unlock(&mutex); 
        // printf ("Timer thread: unlocked mutex, worker not done\n");
        rc = nanosleep(&ts, &tsRem);
        // printf("Timer thread: nanosleep returned %d\n", rc);
        if (rc == 0){
            // printf ("Timer thread: timeout\n");
            printf ("Subtest %d.%d timed out. Terminating.\n", args->testNum, args->subTest);
            kill (args->workerPID, SIGKILL);
        }
    }else{
        pthread_mutex_unlock(&mutex); 
        // printf ("Timer thread: unlocked mutex, worker done\n");
    }

    /* Convert from timeval to timespec */
    // ts.tv_sec = tp.tv_sec;
    // ts.tv_nsec = tp.tv_usec * 1000;
    // ts.tv_sec += args->waitTime;
    // pthread_mutex_lock(&mutex); 
    // printf("Timer thread: locked mutex");
    // while (!workerDone)
    //     rc = pthread_cond_timedwait(&cond, &mutex, &ts);
    // pthread_mutex_unlock(&mutex); 
    // printf("Timer thread: pthread_cond_timedwait returned %d\n", rc);
    // /* If the wait timed out, the main thread in the parent never signalled worker process finishing,
    //    so the worker process must be killed */
    // if (rc == ETIMEDOUT)
    // {
    //     //if (!WIFEXITED(workerStatus) && !WIFSIGNALED(workerStatus)){
    //         printf ("Timer thread: timeout, killing child\n");
    //         kill (args->workerPID, SIGKILL);
    //         //rc = pthread_mutex_unlock(&mutex);
    //     //}
    // }
    // printf("Timer thread: finishing\n");

    return NULL;
}

//Solution with a thread-based timer
void runSubTestWithFileObjType(int testNum, int subTest, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj, int stuff), int testType)
//void runTimedSubTestFileObjType(int testNum, int subTest, int timeOutVal, testRec *rec, char *fileName, void *obj, SubTestRec (*testFunc)(int, int, char *fileName, void *obj, int stuff), int testType)
{
    int timeOutVal = 30;
    int workerParentPipe[2];
    //int workerTimerPipe[2];
    pid_t workerPID;
    int workerStatus;
    pthread_t threadid;
    struct threadArg args;

    signal(SIGUSR1, catchUsr);

    pipe(workerParentPipe); // create the pipe
    workerDone = false;
    workerPID = fork();
    if (workerPID == -1)
    {
        perror("Error: fork worker");
        abort();
    }

    if (workerPID == 0)
    {
        //Worker process - executes sub-test
        // pid_t myPID = getpid();
        // printf("Worker process %d: about to run function", myPID);
        SubTestRec tmpRec = testFunc(testNum, subTest, fileName, obj, testType);
        //printf("worker ran function");
        sendSubDataToParent(workerParentPipe, &tmpRec);
        // printf("Worker process %d: sent data", myPID);
        free(tmpRec.feedback);

        exit(EXIT_SUCCESS);
    }

    //Parent process
    args.waitTime = timeOutVal;
    args.workerPID = workerPID;
    args.testNum = testNum;
    args.subTest = subTest;

    pthread_create(&threadid, NULL, threadfunc, &args);
    // printf("Main thread: started timer\n");

    //Get test case data from child
    SubTestRec tmpRes = getSubDataFromChild(workerParentPipe);
    // printf("Main thread: read from pipe\n");
    
    //Wait for one process
    waitpid(workerPID, &workerStatus, WUNTRACED);
    
    pthread_mutex_lock(&mutex);
    // printf("Main thread: locked mutex\n");

    // printf("Main thread: waitpid returned\n");
    workerDone = true;

    pthread_mutex_unlock(&mutex);    
    // printf("Main thread: unlocked mutex\n");

    // pthread_cond_signal(&cond);
    // printf("Main thread: signalled condition variable\n");

   pthread_kill(threadid, SIGUSR1);
//    printf("Main thread: sent signal\n");

    pthread_join(threadid, NULL);
    // printf("Main thread: timer finished\n");

    char tmpBuf[10000];
    char stuff[1000];

    if (fileName != NULL)
    {
        strcpy(stuff, fileName);
    }
    else
    {
        strcpy(stuff, "NULL");
    }
    if (WIFSIGNALED(workerStatus))
    {
        switch (WTERMSIG(workerStatus))
        {
        case SIGSEGV:
            sprintf(tmpBuf, "Subtest %d.%d (%s) encountered a segmentation fault and crashed", testNum, subTest, stuff);
            break;
        case SIGBUS:
            sprintf(tmpBuf, "Subtest %d.%d (%s) encountered a bus error and crashed", testNum, subTest, stuff);
            break;
        case SIGKILL:
            sprintf(tmpBuf, "Subtest %d.%d (%s) timed out after %d seconds and was terminated (likely cause - infinite loop)", testNum, subTest, stuff, timeOutVal);
            break;
        default:
            sprintf(tmpBuf, "Subtest %d.%d (%s) crashed - killed by a signal %d", testNum, subTest, stuff, WTERMSIG(workerStatus));
            break;
        }
        tmpRes = createSubResult(FAIL, tmpBuf);
    }
    // else if (WEXITSTATUS(workerStatus)){
    //     sprintf(tmpBuf, "Subtest %d.%d (%s) was  terminated by exit() in student code", testNum, subTest, stuff);
    //     tmpRes = createSubResult(FAIL, tmpBuf);
    // }

    addResult(rec, tmpRes.passed, tmpRes.feedback);
    free(tmpRes.feedback);
    if (tmpRes.passed == SUCCESS)
    {
        rec->subsPassed++;
    }
}

/*
void runSubTestWithFileObjType(int testNum, int subTest, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj, int stuff), int testType){
    int pipefd[2];
    pid_t childPID;
    
    pipe(pipefd); // create the pipe
    childPID = fork();
    if(childPID >= 0){ // fork was successful
        if (childPID == 0){
            signal(SIGSEGV, SIG_DFL);
            
            SubTestRec tmpRec = testFunc(testNum, subTest, fileName, obj, testType);
            
            sendSubDataToParent(pipefd, &tmpRec);
            free(tmpRec.feedback);
            exit(EXIT_SUCCESS);
        }else{
            SubTestRec tmpRes = getSubDataFromChild(pipefd);
            
            int status;
            wait(&status);
            char stuff[1000];
            if (fileName != NULL){
                strcpy(stuff, fileName);
            }else{
                strcpy(stuff, "NULL");
            }
            if (WIFSIGNALED(status)){
                char tmpBuf[10000];
                switch (WTERMSIG(status)){
                    case SIGSEGV:
                        sprintf(tmpBuf, "Test %d.%d (%s) encountered a segmentation fault and crashed", testNum, subTest, stuff);
                        break;
                    case SIGBUS:
                        sprintf(tmpBuf, "Test %d.%d (%s) encountered a bus error and crashed", testNum, subTest, stuff);
                        break;
                    default:
                        sprintf(tmpBuf, "Test %d.%d (%s) with PID %d crashed - killed by a signal %d", testNum, subTest, stuff, childPID, WTERMSIG(status));
                        break;
                }
                tmpRes = createSubResult(FAIL, tmpBuf);
            }
            
            addResult(rec, tmpRes.passed, tmpRes.feedback);
            free(tmpRes.feedback);
            if (tmpRes.passed == SUCCESS){
                rec->subsPassed++;
            }
        }
    }
}
*/

void runSubTestWithFileAndObj(int testNum, int subTest, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj)){
    int pipefd[2];
    pid_t childPID;
    
    pipe(pipefd); // create the pipe
    childPID = fork();
    if(childPID >= 0){ // fork was successful
        if (childPID == 0){
            signal(SIGSEGV, SIG_DFL);
            
            SubTestRec tmpRec = testFunc(testNum, subTest, fileName, obj);
            
            sendSubDataToParent(pipefd, &tmpRec);
            free(tmpRec.feedback);
            exit(EXIT_SUCCESS);
        }else{
            SubTestRec tmpRes = getSubDataFromChild(pipefd);
            
            int status;
            wait(&status);
            
            if (WIFSIGNALED(status)){
                char tmpBuf[1000];
                switch (WTERMSIG(status)){
                    case SIGSEGV:
                        sprintf(tmpBuf, "Test %d.%d encountered a segmentation fault and crashed", testNum, subTest);
                        break;
                    case SIGBUS:
                        sprintf(tmpBuf, "Test %d.%d encountered a bus error and crashed", testNum, subTest);
                        break;
                    default:
                        sprintf(tmpBuf, "Test %d.%d crashed - killed by a signal %d", testNum, subTest, WTERMSIG(status));
                        break;
                }
                tmpRes = createSubResult(FAIL, tmpBuf);
            }
            
            addResult(rec, tmpRes.passed, tmpRes.feedback);
            free(tmpRes.feedback);
            if (tmpRes.passed == SUCCESS){
                rec->subsPassed++;
            }
        }
    }
}

void runSubTestWithFile(int testNum, int subTest, testRec* rec, char* fileName, SubTestRec (*testFunc)(int, int, char*fileName)){
    int pipefd[2];
    pid_t childPID;
    
    pipe(pipefd); // create the pipe
    childPID = fork();
    if(childPID >= 0){ // fork was successful
        if (childPID == 0){
            signal(SIGSEGV, SIG_DFL);
            
            SubTestRec tmpRec = testFunc(testNum, subTest, fileName);
            
            sendSubDataToParent(pipefd, &tmpRec);
            free(tmpRec.feedback);
            exit(EXIT_SUCCESS);
        }else{
            SubTestRec tmpRes = getSubDataFromChild(pipefd);
            
            int status;
            wait(&status);
            
            if (WIFSIGNALED(status)){
                char tmpBuf[1000];
                switch (WTERMSIG(status)){
                    case SIGSEGV:
                        sprintf(tmpBuf, "Test %d.%d encountered a segmentation fault and crashed", testNum, subTest);
                        break;
                    case SIGBUS:
                        sprintf(tmpBuf, "Test %d.%d encountered a bus error and crashed", testNum, subTest);
                        break;
                    default:
                        sprintf(tmpBuf, "Test %d.%d crashed - killed by a signal %d", testNum, subTest, WTERMSIG(status));
                        break;
                }
                tmpRes = createSubResult(FAIL, tmpBuf);
            }
            
            addResult(rec, tmpRes.passed, tmpRes.feedback);
            free(tmpRes.feedback);
            if (tmpRes.passed == SUCCESS){
                rec->subsPassed++;
            }
        }
    }
}

void runSubTest(int testNum, int subTest, testRec* rec, SubTestRec (*testFunc)(int, int)){
    int pipefd[2];
    pid_t childPID;
    
    pipe(pipefd); // create the pipe
    childPID = fork();
    if(childPID >= 0){ // fork was successful
        if (childPID == 0){
            signal(SIGSEGV, SIG_DFL);
            
            SubTestRec tmpRec = testFunc(testNum, subTest);
            
            sendSubDataToParent(pipefd, &tmpRec);
            free(tmpRec.feedback);
            exit(EXIT_SUCCESS);
        }else{
            SubTestRec tmpRes = getSubDataFromChild(pipefd);
            
            int status;
            wait(&status);
            
            if (WIFSIGNALED(status)){
                char tmpBuf[1000];
                switch (WTERMSIG(status)){
                    case SIGSEGV:
                        sprintf(tmpBuf, "Test %d.%d encountered a segmentation fault and crashed", testNum, subTest);
                        break;
                    case SIGBUS:
                        sprintf(tmpBuf, "Test %d.%d encountered a bus error and crashed", testNum, subTest);
                        break;
                    default:
                        sprintf(tmpBuf, "Test %d.%d crashed - killed by a signal %d", testNum, subTest, WTERMSIG(status));
                        break;
                }
                tmpRes = createSubResult(FAIL, tmpBuf);
            }
            
            addResult(rec, tmpRes.passed, tmpRes.feedback);
            free(tmpRes.feedback);
            if (tmpRes.passed == SUCCESS){
                rec->subsPassed++;
            }
        }
    }
}

void destroyRecords(testRec ** testRecords, int size)
{
    int i = 0;

    for(i = 0; i < size; i++)
    {
        destroyRecord(testRecords[i]);
    }
}

void destroyRecord(testRec * tmp)
{
    int j = 0;

    if(tmp != NULL)
    {
        for(j = 0; j < tmp->feedbackLen; j++)
        {
            if(tmp->feedback[j] != NULL)
            {
                free(tmp->feedback[j]);
            }
        }

        free(tmp->feedback);
        free(tmp);
    }
}

testRec * initRec(int testNum, int numSubs, char * header)
{
	testRec * newRec = malloc(sizeof(testRec));
	newRec->numSubs = numSubs;
	newRec->feedback = malloc(sizeof(char *)*(numSubs+10)); //10 extra lines for output
	newRec->feedback[0] = malloc(sizeof(char)*(strlen(header)+1));
	strcpy(newRec->feedback[0], header);
	newRec->feedbackLen=1;
	newRec->testNum = testNum;
	newRec->subsPassed = 0;


	return newRec;
}

SubTestRec createSubResult(testResult res, char* feedback){
    SubTestRec result;
    
    result.passed = res;
    result.feedback = malloc(sizeof(char)*(strlen(feedback)+1));
    strcpy(result.feedback, feedback);
    
    return result;
}

void addResult( testRec * rec, testResult res, char * add)
{

	rec->feedback[rec->feedbackLen] = malloc(sizeof(char)*(strlen(add)+20));

	if (res == SUCCESS)
	{
		strcpy(rec->feedback[rec->feedbackLen], GRN"SUCCESS: ");
	}
	else
	{
		strcpy(rec->feedback[rec->feedbackLen], RED"FAIL: ");
	}

	strcat(rec->feedback[rec->feedbackLen],add);
	strcat(rec->feedback[rec->feedbackLen],RESET);
	rec->feedbackLen++;
}


void printRecord (testRec * rec)
{
	int i;

	//printf("subs passed %d, subs total %d",rec->subsPassed, rec->numSubs);
	printf("\n");
	if(rec->subsPassed == rec->numSubs)
	{
		printf(GRN"%s: PASSED %d/%d tests\n"RESET,rec->feedback[0],rec->subsPassed, rec->numSubs); //header for test is always at position 0
	}
	else
	{
		printf(RED"%s: FAILED %d/%d tests\n"RESET,rec->feedback[0],rec->numSubs-rec->subsPassed, rec->numSubs); //header for test is always at position 0

	}
//printf("\n");
	for(i=1; i<rec->feedbackLen; i++)
	{
		printf("    %s\n", rec->feedback[i]);
	}

}

int getScore(testRec * rec)
{
	if(rec->subsPassed == rec->numSubs) return 1;
	else return 0;
}

// Linux-only solutuon with sigtimedwait

/*
void runTimedSubTestFileObjType(int testNum, int subTest, int timeOutVal, testRec *rec, char *fileName, void *obj, SubTestRec (*testFunc)(int, int, char *fileName, void *obj, int stuff), int testType)
{
    int workerParentPipe[2];
    //int workerTimerPipe[2];
    pid_t workerPID;
    pid_t timerPID;
    
    sigset_t mask;
	sigset_t orig_mask;
	struct timespec timeout;
	pid_t pid;
 
	sigemptyset (&mask);
	sigaddset (&mask, SIGUSR1);
 
	if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0) {
		perror ("sigprocmask");
		return;
	}

    setvbuf(stdout, NULL, _IONBF, 0);

    pipe(workerParentPipe); // create the pipe
    //pipe(workerTimerPipe); // create the pipe


    workerPID = fork();
    if (workerPID == -1)
    {
        perror("Error: fork worker");
        abort();
    }

    if (workerPID == 0)
    {
        // pid_t tmpPID;
        // close(workerTimerPipe[1]); // close the write-end of the pipe, worker will read
        // read(workerTimerPipe[0], &tmpPID, sizeof(pid_t));

        //Worker process - executes sub-test
        //printf("worker about to run function");
        SubTestRec tmpRec = testFunc(testNum, subTest, fileName, obj, testType);
        //printf("worker ran function");
        sendSubDataToParent(workerParentPipe, &tmpRec);
        //printf("worker sent data");
        free(tmpRec.feedback);

        //Tell timer that worker is finished
        // kill(tmpPID, SIGCHLD);

        exit(EXIT_SUCCESS);
    }

    timerPID = fork();
    if (timerPID == -1)
    {
        perror("Error: fork timer");
        abort();
    }

    /// Timer process
    if (timerPID == 0)
    {
        close(workerParentPipe[0]);
        close(workerParentPipe[1]);

        timeout.tv_sec = timeOutVal;
	    timeout.tv_nsec = 0;

        // close(workerTimerPipe[0]); // close the read-end of the pipe, timer will write
        // pid_t myPID = getpid();
        // write(workerTimerPipe[1], &myPID, sizeof(pid_t));

        int retSignal;

        do {
            retSignal = sigtimedwait(&mask, NULL, &timeout);
            if (retSignal < 0) {
                if (errno == EINTR) {
                    // Interrupted by a signal other than SIGCHLD.
                    continue;
                }
                else if (errno == EAGAIN) {
                    printf ("Timeout, killing child\n");
                    kill (workerPID, SIGKILL);
                }
                else {
                    perror ("sigtimedwait");
                    return;
                }
            }
    
            break;
        } while (1);
        printf ("Timer done\n");
        exit(0);
    }

    //Parent process

    //Parent does not need the worker-timer pipe
    // close(workerTimerPipe[0]);
    // close(workerTimerPipe[1]);


    //Get test case data from child
    printf("Worker: %d\nTimer: %d\n", workerPID, timerPID);
    printf("*******\n");
    SubTestRec tmpRes = getSubDataFromChild(workerParentPipe);
    printf("!!!!!!!\n");
    int status;
    
    //Wait for one process
    waitpid(workerPID, &status, WUNTRACED);
    kill(timerPID, SIGUSR1);
    // printf("About to kill %d\n", timerPID);
    // kill(timerPID, SIGFPE);
    // printf("Killed %d\n", timerPID);

    char tmpBuf[10000];
    char stuff[1000];

    if (fileName != NULL)
    {
        strcpy(stuff, fileName);
    }
    else
    {
        strcpy(stuff, "NULL");
    }
    if (WIFSIGNALED(status))
    {
        switch (WTERMSIG(status))
        {
        case SIGSEGV:
            sprintf(tmpBuf, "Subtest %d.%d (%s) encountered a segmentation fault and crashed", testNum, subTest, stuff);
            break;
        case SIGBUS:
            sprintf(tmpBuf, "Subtest %d.%d (%s) encountered a bus error and crashed", testNum, subTest, stuff);
            break;
        case SIGKILL:
            sprintf(tmpBuf, "Subtest %d.%d (%s) timed out after %d seconds and was terminated (likely cause - infinite loop)", testNum, subTest, stuff, timeOutVal);
            break;
        default:
            sprintf(tmpBuf, "Subtest %d.%d (%s) crashed - killed by a signal %d", testNum, subTest, stuff, WTERMSIG(status));
            break;
        }
        tmpRes = createSubResult(FAIL, tmpBuf);
    }

    addResult(rec, tmpRes.passed, tmpRes.feedback);
    free(tmpRes.feedback);
    if (tmpRes.passed == SUCCESS)
    {
        rec->subsPassed++;
    }
}

*/