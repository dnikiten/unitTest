#include "SampleHeader.h"
#include <stdio.h>

int main(void){

    printf("Arithmetic");
    printf("add2 %d+2 = %d\n", 10, add2(12));
    printf("add2buggy %d+2 = %d\n", 10, add2buggy(12));

    printf("\n\nMemory");
    int* testArr;
    testArr = myAlloc(sizeof(int), 10);
    printf("%p\n", testArr);
    free(testArr);
    
    testArr = myAllocBuggy1(sizeof(int), 10);
    printf("%p\n", testArr);
    free(testArr);

    testArr = myAllocBuggy2(sizeof(int), 10);
    printf("%p\n", testArr);
    free(testArr);

    return 0;
}