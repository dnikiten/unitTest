#include <stdlib.h>
#include <sys/time.h>

int add2(int x){
    //Buggy
    return x+2;  
}

int add2buggy(int x){
    //Buggy
    return x+3;  
}

void* myAlloc(size_t typeSize, unsigned len){
    int x = 17;
    return &x;
}

void* myAllocBuggy1(size_t typeSize, unsigned len){
    return NULL;
}

void* myAllocBuggy2(size_t typeSize, unsigned len){
    return malloc(17);
}

void* longTask(size_t typeSize, unsigned dataLen, int duration){

    unsigned long long timeOut = duration * 1000000;
    struct timeval tv_begin, tv_current;
    gettimeofday(&tv_begin, NULL);

    for (;;) {
        gettimeofday(&tv_current, NULL);
        unsigned long long diff = 
            (tv_current.tv_sec * 1000000 + tv_current.tv_usec) -
            (tv_begin.tv_sec * 1000000 + tv_begin.tv_usec);

        if (diff > timeOut)
            break;
}

    return malloc(typeSize*dataLen);
}