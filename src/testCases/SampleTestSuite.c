#include "TestHarness.h"
#include "SampleTestSuite.h"
#include "SampleHeader.h"
#include <math.h>
#include <time.h>

//Take a struct and a test function, apply a test function to struct
enum ApiFuncs {ADD_2, ADD_2_BUGGY, MY_ALLOC, MY_ALLOC_BUGGY_1, MY_ALLOC_BUGGY2, LONG_TASK, NUM_FUNCS};

static char* typeStr[] = {
    "integer", "integer", "memory block", "memory block", "memory block", "memory block after a lot of calcluations"
};

static char* funcNames[] = {
    "add2", "add2buggy", "myAlloc", "myAllocBuggy1", "myAllocBuggy2", "longTask"
};

typedef struct {
    int input;
    int refRes;
} StructTestArg;

typedef struct {
    int numElements;
    char* testVal;
} StructTestNameSearch;

static SubTestRec applyNumTest(int testNum, int subTest, char* fileName, void* obj, int testType){
    SubTestRec result;
    char feedback[3000];
    
    StructTestArg* testData;
    
    int refVal;
    int testVal;
    // void* testPtr;
   //char wptName[100] = "";

    switch(testType){
        case ADD_2:
            testData = (StructTestArg*)obj;
            refVal = testData->refRes;
            testVal = add2(testData->input);
            break;
        case ADD_2_BUGGY:
            testData = (StructTestArg*)obj;
            refVal = testData->refRes;
            testVal = add2buggy(testData->input);
            break;
        default:
            printf("Invalid test case! %d\n", testType);
            exit(0);
    }

    char outStr[100];

    if (testVal == refVal){
        sprintf(outStr, "%s returned corect %s %d", funcNames[testType], typeStr[testType], refVal);
        sprintf(feedback, "Subtest %d.%d: passed (%s)", testNum, subTest, outStr);

        result = createSubResult(SUCCESS, feedback);
        return result;
    }else{
        sprintf(outStr, "%s returned incorrect %s %d - expected %d", funcNames[testType], typeStr[testType], testVal, refVal);
        sprintf(feedback, "Subtest %d.%d: failed (%s)", testNum, subTest, outStr);

        result = createSubResult(FAIL, feedback);
        return result;
    }

    return result;
}

// A simple generic addition test
static testRec* _tTestAddGeneric(int testNum, int inpNum, int expRes, char* str, enum ApiFuncs testType){
    const int numSubs = 1;
    int subTest = 1;
    char feedback[3000];

    sprintf(feedback, "%s", str);
    testRec* rec = initRec(testNum, numSubs, feedback);

    StructTestArg testNumData;
    testNumData.input = inpNum;
    testNumData.refRes = expRes;
    runSubTestWithFileObjType(testNum, subTest, rec, NULL, &testNumData, applyNumTest, testType);

    return rec;
}

//Individual test case - should pass
testRec* _tTestAdd1(int testNum){
    return _tTestAddGeneric(testNum, 10, 12, "Testing add2", ADD_2);
}

//Individual test case - should fail
testRec* _tTestAdd2(int testNum){
    return _tTestAddGeneric(testNum, 10, 12, "Testing add2buggy", ADD_2_BUGGY);
}

//Several sub-tests in once case. One will pass, one will fail
testRec* _tTestAddMultiple(int testNum){
    const int numSubs = 2;
    int subTest = 1;
    char feedback[3000];
    
    sprintf(feedback, "Testing functions add2 and add2buggy  ");
    testRec* rec = initRec(testNum, numSubs, feedback);
    
    StructTestArg testNumData;
    testNumData.input = 10;
    testNumData.refRes = 12;
    runSubTestWithFileObjType(testNum, subTest, rec, NULL, &testNumData, applyNumTest, ADD_2);

    subTest += 1;
    testNumData.input = 10;
    testNumData.refRes = 12;
    runSubTestWithFileObjType(testNum, subTest, rec, NULL, &testNumData, applyNumTest, ADD_2_BUGGY);

    return rec;
}