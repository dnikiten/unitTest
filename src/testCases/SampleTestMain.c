#include "SampleTestSuite.h"

#define TESTS 3
#define DEBUG 1
#define OUT stdout

int testNo;
testRec * * testRecords;
int studentScore;  //globals  required to handle segfaults gracefully

void addTestResult(testRec* tmpRec){
    testRecords[testNo-1] = tmpRec;
    studentScore = studentScore + getScore(testRecords[testNo-1]);
    testNo++;
}

float calcGrade(void){
    float weights[] = {
        1,1,1

    };
    float totalScore = 0;
    int i = 0;
    for (i = 0; i < TESTS; i++){
        totalScore += weights[i]*(float)getScore(testRecords[i]);
    }
    return totalScore;
}

int main(int argc, char* argv[]){
{
    
    studentScore = 0;
    testNo = 1;
    testRec* tmpRec = NULL;
    
    //record keeping array
    testRecords =  malloc(sizeof(testRec *)* TESTS);
    
    if(DEBUG) fprintf(OUT, "************** Testing Details ********************\n\n");

    tmpRec = _tTestAdd1(testNo);
    addTestResult(tmpRec);

    tmpRec = _tTestAdd2(testNo);
    addTestResult(tmpRec);

    tmpRec = _tTestAddMultiple(testNo);
    addTestResult(tmpRec);

    int j;
    for(j=0; j<TESTS; j++)
    {

        if (j == 0) {
            printf("\n\nSAMPLE LIBRARY TESTING\n");
            printf("\nPreliminary add... tests\n");
        }

        if (j == 2) {
            printf("\nAdditional add... tests\n");
        }

        printRecord(testRecords[j]);
        //printf("\n");
    }
    //fclose(output);
    printf("Score: %.0f/3 test blocks contain no errors\n", calcGrade());
    
    return 0;
    
}
}