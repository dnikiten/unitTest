#include <stdlib.h>
#include <sys/time.h>

int add2(int x);
int add2buggy(int x);
void* myAlloc(size_t typeSize, unsigned len);
void* myAllocBuggy1(size_t typeSize, unsigned len);
void* myAllocBuggy2(size_t typeSize, unsigned len);
void* longTask(size_t typeSize, unsigned dataLen, int duration);