#ifndef _TEST_HARNESS_
#define _TEST_HARNESS_	
//#define _POSIX_C_SOURCE	199309L
#define _POSIX_C_SOURCE 200809L

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <pthread.h>
#include <sys/time.h>

#include <stdbool.h>

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define RESET "\x1B[0m"

#define SUCCESS 1
#define FAIL 0

typedef int testResult;

typedef struct {
    testResult passed;
    char* feedback;
} SubTestRec;

typedef struct testRecord
{
	int numSubs;
	int testNum;
	int subsPassed;

	char ** feedback;
	int feedbackLen;
}testRec;

SubTestRec getSubDataFromChild(int* pipefd);
void sendSubDataToParent(int* pipefd, SubTestRec* tmpRec);
void runSubTest(int testNum, int subTest, testRec* rec, SubTestRec (*testFunc)(int, int));
void runSubTestWithFile(int testNum, int subTest, testRec* rec, char* fileName, SubTestRec (*testFunc)(int, int, char* fileName));
void runSubTestWithFileAndObj(int testNum, int subTest, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj));
void runSubTestWithFileObjType(int testNum, int subTest, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj, int stuff), int testType);

// Sub-test with a time-out.  Re-write to pass arguments as a struct 
void runTimedSubTestFileObjType(int testNum, int subTest, int timeOut, testRec* rec, char* fileName, void* obj, SubTestRec (*testFunc)(int, int, char*fileName, void* obj, int stuff), int testType);

testRec * initRec(int testNum, int numSubs, char * header);
void addResult( testRec * rec, testResult res, char * add);
SubTestRec createSubResult(testResult res, char* feedback);
void printRecord (testRec * rec);
int getScore(testRec * rec);
void destroyRecords(testRec ** testRecords, int size);
void destroyRecord(testRec * tmp);

#endif

