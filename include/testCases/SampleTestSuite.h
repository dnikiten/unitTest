#ifndef SAMPLETESTSUITE_H
#define SAMPLETESTSUITE_H

#include "TestHarness.h"


testRec* _tTestAdd1(int testNum);
testRec* _tTestAdd2(int testNum);
testRec* _tTestAddMultiple(int testNum);

#endif