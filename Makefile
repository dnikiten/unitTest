UNAME := $(shell uname)

CFLAGS = -Wall -g -std=c11

BIN = bin/

HARN_INC = include/harness/
SOLN_INC = include/sampleSolutionCode/
TEST_INC = include/testCases/

HARN_SRC = src/harness/
SOLN_SRC = src/sampleSolutionCode/
TEST_SRC = src/testCases/

ALL_INC = -I$(TEST_INC) -I$(SOLN_INC) -I$(HARN_INC)

# Shared lib
myTestCode: $(BIN)libmyTestCode.so

$(BIN)libmyTestCode.so: $(SOLN_SRC)/SampleSource.c $(SOLN_INC)/SampleHeader.h
	gcc $(CFLAGS) $(ALL_INC) -shared -o $(BIN)libmyTestCode.so $(SOLN_SRC)/SampleSource.c -lm


#Sample main for the lib. Can crash if buggy calls are commended out.
demo: myTestCode $(SOLN_SRC)SampleMain.c $(SOLN_INC)SampleHeader.h
ifeq ($(UNAME), Linux)
	gcc $(CFLAGS) $(ALL_INC) -L$(BIN) $(SOLN_SRC)SampleMain.c -lm -lmyTestCode -o $(BIN)demo
endif
ifeq ($(UNAME), Darwin)
	gcc $(CFLAGS) $(ALL_INC) -L$(BIN) $(SOLN_SRC)SampleMain.c -lxml2 -lm -lmyTestCode -o $(BIN)demo; \
	install_name_tool -change $(BIN)libmyTestCode.so libmyTestCode.so $(BIN)demo; \
	install_name_tool -id libmyTestCode.so $(BIN)libmyTestCode.so
endif

TEST_FILES_A1 = $(TEST_SRC)*.c $(HARN_SRC)TestHarness.c

#Sample unit test main.
test1: myTestCode $(TEST_FILES_A1)
ifeq ($(UNAME), Linux)
	gcc $(CFLAGS) $(ALL_INC) $(TEST_FILES_A1) -o $(BIN)test1 -L$(BIN) -lmyTestCode -lm -lpthread
endif
ifeq ($(UNAME), Darwin)
	gcc $(CFLAGS) $(ALL_INC) $(TEST_FILES_A1) -o $(BIN)test1 -L$(BIN) -lmyTestCode -lm -lpthread; \
	install_name_tool -change $(BIN)libmyTestCode.so libmyTestCode.so $(BIN)test1; \
	install_name_tool -id libmyTestCode.so $(BIN)libmyTestCode.so
endif

clean:
	rm -rf $(BIN)*.o $(BIN)*.so test* demo* mem*